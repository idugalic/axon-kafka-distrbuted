package com.techm.bm.view.query;

import lombok.Value;

import java.util.UUID;

@Value
public class FindOrderStatus {

    UUID orderId;

}
