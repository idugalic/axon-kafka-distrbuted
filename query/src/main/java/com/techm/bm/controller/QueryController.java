package com.techm.bm.controller;

import java.util.List;
import java.util.UUID;

import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.techm.bm.repository.OrderStatusRepository;
import com.techm.bm.view.model.OpenShipment;
import com.techm.bm.view.model.OrderStatus;
import com.techm.bm.view.query.FindOrderStatus;
import com.techm.bm.view.query.ListAll;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("bm/query")
@RequiredArgsConstructor
public class QueryController {

	private final QueryGateway queryGateway;

	@Autowired
	private OrderStatusRepository orderStatusRepository;

	@ApiOperation(value = "Find order status by orderId", notes = "Find order status by orderId")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Order detatails fetched"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	@RequestMapping(path = "/order/status/{orderId}", method = RequestMethod.GET)
	public OrderStatus findStatus(String orderId) {
		return queryGateway
				.query(new FindOrderStatus(UUID.fromString(orderId)), ResponseTypes.instanceOf(OrderStatus.class))
				.join();
	}

	@ApiOperation(value = "Fetch all order", notes = "Fetch all order")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Order detatails fetched"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	@RequestMapping(path = "/orders", method = RequestMethod.GET)
	public List<OrderStatus> fetchOrders() {
		return orderStatusRepository.findAll();
	}

	@ApiOperation(value = "Find all open shipments", notes = "Find all open shipments")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Open shipment detatails fetched"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	@RequestMapping(path = "/shipments/open", method = RequestMethod.GET)
	public List<OpenShipment> findOpen() {
		return queryGateway.query(new ListAll(), ResponseTypes.multipleInstancesOf(OpenShipment.class)).join();
	}
}
