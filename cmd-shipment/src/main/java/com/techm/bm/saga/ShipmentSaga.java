package com.techm.bm.saga;

import static org.axonframework.modelling.saga.SagaLifecycle.associateWith;

import java.util.UUID;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.config.ProcessingGroup;
import org.axonframework.modelling.saga.EndSaga;
import org.axonframework.modelling.saga.SagaEventHandler;
import org.axonframework.modelling.saga.StartSaga;
import org.axonframework.spring.stereotype.Saga;
import org.springframework.beans.factory.annotation.Autowired;

import com.techm.bm.api.cmd.PrepareShipmentCmd;
import com.techm.bm.api.cmd.RegisterShipmentForOrderArrivedCmd;
import com.techm.bm.api.cmd.RegisterShipmentForOrderPreparedCmd;
import com.techm.bm.api.evt.OrderPlacedEvt;
import com.techm.bm.api.evt.ShipmentArrivedEvt;
import com.techm.bm.api.evt.ShipmentPreparedEvt;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Saga
@ProcessingGroup("command-processor")
public class ShipmentSaga {

	@Autowired
	private transient CommandGateway commandGateway;

	private UUID orderId;
	
	private UUID shipmentId;


	@StartSaga
	@SagaEventHandler(associationProperty = "orderId")
	public void on(OrderPlacedEvt evt) {
		log.debug("handling {}", evt);
		orderId = evt.getOrderId();
		shipmentId = UUID.randomUUID();
		log.debug("shipmentId: {}", shipmentId);
		associateWith("shipmentId", shipmentId.toString());
		commandGateway.send(new PrepareShipmentCmd(shipmentId, evt.getDestination()));
	}

	@SagaEventHandler(associationProperty = "shipmentId")
	public void on(ShipmentPreparedEvt evt) {
		log.debug("handling {}", evt);
		log.debug("orderId: {}", orderId);
		log.debug("shipmentId: {}", shipmentId);
		commandGateway.send(new RegisterShipmentForOrderPreparedCmd(orderId, evt.getShipmentId()));
	}

	@SagaEventHandler(associationProperty = "shipmentId")
	@EndSaga
	public void on(ShipmentArrivedEvt evt) {
		log.debug("handling {}", evt);
		log.debug("orderId: {}", orderId);
		log.debug("shipmentId: {}", evt.getShipmentId());
		commandGateway.send(new RegisterShipmentForOrderArrivedCmd(orderId, evt.getShipmentId()));
	}
}