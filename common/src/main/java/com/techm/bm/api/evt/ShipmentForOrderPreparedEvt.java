package com.techm.bm.api.evt;

import lombok.Value;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.UUID;

@Value
public class ShipmentForOrderPreparedEvt {

    @TargetAggregateIdentifier
    UUID orderId;

    UUID shipmentId;

}
