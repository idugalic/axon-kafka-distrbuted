package com.techm.bm.controller;

import java.util.UUID;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.techm.bm.api.cmd.PlaceOrderCmd;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("bm/command")
@RequiredArgsConstructor
public class CommandController {

    private final CommandGateway commandGateway;

	@ApiOperation(value = "Place order", notes = "Send command to place order")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Order placed"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
    @RequestMapping(path = "/order/placeOrder", method = RequestMethod.POST)
    public UUID placeOrder(String goods, String destination) {
        return commandGateway.sendAndWait(new PlaceOrderCmd(UUID.randomUUID(), goods, destination));
    }
}
